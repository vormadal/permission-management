package dk.agenia.permissionmanagement.models;

import lombok.Data;

/**
 * <p>Created: 05-10-2018</p>
 * <p>author: Runi</p>
 */

@Data
public class LoginData {
    private String username;
    private String password;
}
